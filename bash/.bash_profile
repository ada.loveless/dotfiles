if [ -f /etc/profile ]
then
    source /etc/profile
fi
if [ -f "${HOME}/.bashrc" ]
then
    source "${HOME}/.bashrc"
fi
