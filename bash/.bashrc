isinteractive="$(expr index $- i)"
source_if() {
    if [ -f "$1" ]
    then
        source "$1"
    fi
}

# Global bashrc
source_if /etc/bashrc

# Some completions
source_if /etc/bash_completion.d/git
source_if /usr/share/bash-completion/completions/apptainer
source_if /usr/share/bash-completion/completions/singularity
source_if /usr/share/bash-completion/completions/pip3

# History
export HISTFILESIZE=10000
export HISSIZE=1000
export HISTCONTROL=erasedups:ignoredups:ignorespace

# Options
shopt -s checkwinsize # resize lines when terminal resize
shopt -s histappend # Append to history
PROMPT_COMMAND='history -a'
stty -ixon # Ctrl-S for history navigation
stty werase undef # Undefine the C-w word deleteion. Redefined in .inputrc to be closer to zsh behaviour
set -o vi # Set vi mode

# Editor
export EDITOR="vim"
export VISUAL="vim"

# Colors
export CLICOLOR=1
export LSCOLORS='no=00:fi=00:di=00;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:*.xml=00;31:'
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# Aliases
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -v"
alias ls="ls --color=auto"
alias l="ls -hals"
alias la='ls -Alh' # show hidden files
alias ll='ls -Fls' # long listing format
alias less="less -R"
alias ..="cd .."
alias bd="cd $OLDPWD"

# Prompt
RED="\[$(tput setaf 1)\]"
GREEN="\[$(tput setaf 2)\]"
YELLOW="\[$(tput setaf 3)\]"
BLUE="\[$(tput setaf 4)\]"
MAGENTA="\[$(tput setaf 5)\]"
CYAN="\[$(tput setaf 6)\]"
RESET="\[$(tput sgr0)\]"

exitstatus() {
    if [[ $? == 0 ]];
    then printf ''
    else echo "$?"
    fi
}

PS1="${BLUE}$? ${CYAN}${USER} ${YELLOW}\h ${GREEN}\W${RESET} \$ "

export PATH=$PATH:$HOME/bin
export PATH=$PATH:$HOME/.local/bin

module use /omics/groups/OE0147/internal/software/.modules
module add zellij/0.38.2

# Better binds
if [[ $isinteractive > 0 ]]
then
    bind -f "$HOME/.inputrc"
fi
