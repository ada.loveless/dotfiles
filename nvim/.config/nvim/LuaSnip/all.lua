local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras.fmt").rep

return {
  -- A snippet that expands the trigger "hi" into the string "Hello, world!".
  s({ trig = "ifn", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("interferon")),
  s({ trig = "Ifn", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Interferon")),
  s({ trig = "IFN", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Interferon")),
  s({ trig = "nsc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("neural stem cell")),
  s({ trig = "qnsc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("quiescent neural stem cell")),
  s({ trig = "ansc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("active neural stem cell")),
  s({ trig = "tap", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("transient amplifying progenitor")),
  s({ trig = "Nsc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Neural stem cell")),
  s({ trig = "Qnsc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Quiescent neural stem cell")),
  s({ trig = "Ansc", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Active neural stem cell")),
  s({ trig = "Tap", priority = 100, wordTrig = true, snippetType = "autosnippet" }, t("Transient amplifying progenitor")),
}
-- vim: sw=2
