local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras.fmt").rep

return {
  s({ trig = "tab", dscr = "A LaTeX table environment", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{table}[<>]
        \begin{tabular}[c c c]
          \toprule
          a & b & c\\
          \midrule
          <>
          \bottomrule
        \end{tabular}
        \caption{%
        }\label{tab:<>}
      \end{table}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1, "htbp"), i(0), i(2) }
    )
  ),
  s({ trig = "frame", dscr = "A beamer frame environment", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{frame}[<>]{<>}
        <>
      \end{frame}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1, "t"), i(2), i(0) }
    )
  ),
  s({ trig = "block", dscr = "A beamer block environment", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{block}{<>}
        <>
      \end{block}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(0) }
    )
  ),
  s({ trig = "col2", dscr = "A beamer columns environment with two columns", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{columns}[t]
        \begin{column}[t]{.45\textwidth}
          <>
        \end{column}
        \begin{column}[t]{.45\textwidth}
          <>
        \end{column}
      \end{columns}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(2), }
    )
  ),
  s({ trig = "col3", dscr = "A beamer columns environment with three columns", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{columns}[t]
        \begin{column}[t]{.3\textwidth}
          <>
        \end{column}
        \begin{column}[t]{.3\textwidth}
          <>
        \end{column}
        \begin{column}[t]{.3\textwidth}
          <>
        \end{column}
      \end{columns}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(2), i(3) }
    )
  ),
  s({ trig = "bcol2", dscr = "A beamer columns environment with three columns and blocks", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{columns}[t]
        \begin{column}[t]{.45\textwidth}
          \begin{block}{<>}
            <>
          \end{block}
        \end{column}
        \begin{column}[t]{.45\textwidth}
          \begin{block}{<>}
            <>
          \end{block}
        \end{column}
      \end{columns}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(2), i(3), i(4) }
    )
  ),
  s({ trig = "tikz", dscr = "A tikzpicture environment", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{tikzpicture}[
          <>
        ]
        <>
      \end{tikzpicture}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(0) }
    )
  ),
  s({ trig = "axis", dscr = "An axis environment", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      \begin{axis}[
          <>
        ]
        <>
      \end{axis}
      ]],
      -- The insert node is placed in the <> angle brackets
      { i(1), i(0) }
    )
  ),
  s({ trig = "tikzfile", dscr = "A tikzpicture file", priority = 100 },
    fmta( -- The snippet code actually looks like the equation environment it produces.
      [[
      % arara: xelatex: { interaction: nonstopmode }
      \documentclass[tikz]{standalone}
      \usepackage{mathtools}
      \usepackage{tikz}
      \usepackage{pgfplots}
      \begin{document}
      \begin{tikzpicture}[
          <>
        ]
        <>
      \end{tikzpicture}
      \end{document}
      % vim: ft=tex sw=2 nospell
      ]],

      -- The insert node is placed in the <> angle brackets
      { i(1), i(0) }
    )
  ),
}
-- vim: sw=2
