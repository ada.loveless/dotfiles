-- generic options
vim.opt.nu = true
vim.opt.rnu = true
vim.opt.mouse = "a"
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.timeout = true
vim.opt.timeoutlen = 500
vim.opt.cursorline = true

-- Default indentation rules
vim.opt.et = true
vim.opt.sw = 4
vim.opt.ts = 4

vim.api.nvim_command("autocmd TermOpen * setlocal nonu")  -- no numbers
vim.api.nvim_command("autocmd TermOpen * setlocal nornu") -- no numbers

-- prevent typo when pressing `wq` or `q`
vim.cmd [[
cnoreabbrev <expr> W ((getcmdtype() is# ":" && getcmdline() is# "W")?("w"):("W"))
cnoreabbrev <expr> Q ((getcmdtype() is# ":" && getcmdline() is# "Q")?("q"):("Q"))
cnoreabbrev <expr> WQ ((getcmdtype() is# ":" && getcmdline() is# "WQ")?("wq"):("WQ"))
cnoreabbrev <expr> Wq ((getcmdtype() is# ":" && getcmdline() is# "Wq")?("wq"):("Wq"))
]]

-- Leader is space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Packages
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- Plugin configs
local plugins = {
    -- Common sense settings
    "tpope/vim-sensible",
    -- Style plugins and such
    {
        "navarasu/onedark.nvim",
        lazy = true,
        opts = {
            style = "warmer",
            transparent = true,
            term_colors = true,
            ending_tildes = false,
            lualine = {
                transparent = false,
            },
            diagnostics = {
                darker = true,
                undercurl = true,
                background = true,
            },
            colors = {
                bg0 = "#333333",
                bg1 = "#393939",
                bg2 = "#383838",
                bg3 = "#3a3a3a",
                bg_d = "#2c2c2c",
                fg = "#f4f4f4",
                red = "#de4d68",
                dark_red = "#a34141",
                green = "#8ad454",
                yellow = "#ead858",
                blue = "#57a5e5",
                purple = "#bb70d2",
                dark_purple = "#79428a",
                cyan = "#51a8b3",
                dark_cyan = "#328f96",
                orange = "#ee9542",
                light_grey = "#848484",
                grey = "#777777",
                diff_delete = "#5e4040",
                diff_change = "#2d4a61",
                diff_add = "#313c28",
            },
        },
    },
    {
        "nvim-lualine/lualine.nvim",
        opts = {
            options = {
                icons_enabled = true,
                theme = "auto",
                component_separators = { left = "", right = "" },
                section_separators = { left = "", right = "" },
                disabled_filetypes = {
                    statusline = {},
                    winbar = {},
                },
                ignore_focus = {},
                always_divide_middle = true,
                globalstatus = false,
                refresh = {
                    statusline = 1000,
                    tabline = 1000,
                    winbar = 1000,
                }
            },
            sections = {
                lualine_a = { "mode" },
                lualine_b = { "branch", "diff", "diagnostics" },
                lualine_c = { "filename" },
                lualine_x = { "encoding", "fileformat", "filetype" },
                lualine_y = { "progress" },
                lualine_z = { "location" }
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { "filename" },
                lualine_x = { "location" },
                lualine_y = {},
                lualine_z = {}
            },
            tabline = {},
            winbar = {},
            inactive_winbar = {},
            extensions = {}
        },
        dependencies = { "nvim-tree/nvim-web-devicons" }
    },
    {
        "nanozuki/tabby.nvim",
        opts = {
            nerdfont = true,
            lualine_theme = true,
            buf_name = {
                mode = "unique"
            },
        },
        config = function(_, opts)
            vim.o.showtabline = 2
            require("tabby.tabline").use_preset("active_wins_at_tail", opts)
        end,
    },
    -- Git plugins
    {
        "lewis6991/gitsigns.nvim",
        event = "VeryLazy",
        opts = {
            signs = {
                add = { text = "▎" },
                change = { text = "▎" },
                delete = { text = "" },
                topdelete = { text = "" },
                changedelete = { text = "▎" },
                untracked = { text = "▎" },
            },
        },
        keys = {
            { "<leader>hS", "<Cmd>Gitsigns stage_buffer<CR>",         desc = "Stage buffer" },
            { "<leader>hu", "<Cmd>Gitsigns undo_stage_hunk<CR>",      desc = "Undo stage" },
            { "<leader>hR", "<Cmd>Gitsigns reset_buffer<CR>",         desc = "Reset buffer" },
            { "<leader>hp", "<Cmd>Gitsigns preview_hunk<CR>",         desc = "Preview hunk" },
            { "<leader>hb", "<Cmd>Gitsigns blame_line full=true<CR>", desc = "Blame line" },
            { "<leader>hd", "<Cmd>Gitsigns diffthis<CR>",             desc = "Diff this" },
            { "<leader>hD", "<Cmd>Gitsigns diffthis ~<CR>",           desc = "Diff this" },
            { "<leader>hs", ":Gitsigns stage_hunk<CR>",               desc = "Stage hunk",    mode = { "n", "v" } },
            { "]h",         ":Gitsigns next_hunk<CR>",                desc = "Next hunk",     mode = { "n", "v" } },
            { "[h",         ":Gitsigns prev_hunk<CR>",                desc = "Previous hunk", mode = { "n", "v" } },
            { "ih",         "<Cmd>:<C-U>Gitsigns select_hunk<CR>",    desc = "Select hunk",   mode = { "o", "x" } },
        },
    },
    "tpope/vim-fugitive",
    -- File browsing and such
    {
        "nvim-tree/nvim-tree.lua",
        opts = {
            view = {
                width = 40,
            },
            filters = {
                dotfiles = true,
            }
        },
        lazy = true,
        keys = {
            { "<leader>tt", "<Cmd>:NvimTreeToggle<CR>",  desc = "Toggle Tree" },
            { "<leader>tr", "<Cmd>:NvimTreeRefresh<CR>", desc = "Refresh Tree" },
        },
        dependencies = { "nvim-tree/nvim-web-devicons" }
    },
    {
        "nvim-telescope/telescope.nvim",
        version = "0.1.4",
        dependencies = { "nvim-lua/plenary.nvim" },
        lazy = true,
        keys = {
            { "<leader>ff",  "<Cmd>:Telescope find_files<CR>",           desc = "Find Files" },
            { "<leader>fg",  "<Cmd>:Telescope live_grep<CR>",            desc = "Live Grep" },
            { "<leader>fb",  "<Cmd>:Telescope buffers<CR>",              desc = "Buffers" },
            { "<leader>fh",  "<Cmd>:Telescope help_tags<CR>",            desc = "Search Help" },
            { "<leader>fH",  "<Cmd>:Telescope keymaps<CR>",              desc = "Search Keymaps" },
            { "<leader>fd",  "<Cmd>:Telescope diagnostics<CR>",          desc = "Search Diagnostics" },
            { "gr",          "<Cmd>:Telescope lsp_references<CR>",       desc = "LSP references" },
            { "<leader>fD",  "<Cmd>:Telescope lsp_document_symbols<CR>", desc = "Search Document Symbols" },
            { "<leader>fy",  "<Cmd>:Telescope registers<CR>",            desc = "Search Registers" },
            { "<leader>fGC", "<Cmd>:Telescope git_commits<CR>",          desc = "Search Git Commits" },
            { "<leader>fGS", "<Cmd>:Telescope git_status<CR>",           desc = "Search Git Status" },
            -- { "<leader>z=",  "<Cmd>:Telescope spell_suggest<CR>",        desc = "Suggest spelling" },
        },
    },
    {
        "simrat39/symbols-outline.nvim",
        opts = {
            highlight_hovered_item = true,

        },
        keys = {
            { "<leader>so", "<cmd>:SymbolsOutline<CR>", desc = "Outline (symbols)" },
        },
    },
    {
        "stevearc/dressing.nvim",
        opts = {
            input = {
                enabled = true,
                title_pos = "left",
                border = "rounded",
                buf_options = {},
                win_options = {
                    wrap = false,
                    list = true,
                },
            },
            select = {
                backend = "telescope",
            },
            builtin = {
                relative = "win",
            }
        },
    },
    -- Language support!
    "neovim/nvim-lspconfig",
    {
        "ray-x/lsp_signature.nvim",
        opts = {
            bind = true,
            floating_window = true,
            hint_enable = false,
            handler_opts = {
                border = "rounded",
            },
        }
    },
    {
        -- LuaSnip for my own snippets and such
        "L3MON4D3/LuaSnip",
        opts = {
            enable_autosnippets = true,
        },
        dependencies = {
            "rafamadriz/friendly-snippets", -- Optional
        },
        config = function(_, opts)
            require("luasnip.loaders.from_lua").load { paths = "~/.config/nvim/LuaSnip/" }
            require("luasnip.loaders.from_vscode").lazy_load()
            require("luasnip").config.setup(opts)
            require("which-key").register({
                ["<C-Tab>"] = { "<Plug>luasnip-jump-next", "Jump to next insert" },
            }, { mode = { "i", "s" } })
        end,
    },
    {
        -- Mason for installing LSPSs.
        "williamboman/mason.nvim",
        lazy = false,
        dependencies = { "williamboman/mason-lspconfig.nvim", },
        config = function(_, _)
            require("mason").setup()
            local lspconfig = require("lspconfig")
            local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()
            local default_setup = function(server)
                lspconfig[server].setup({
                    capabilities = lsp_capabilities,
                })
            end
            require("mason-lspconfig").setup {
                ensure_installed = { "texlab", "lua_ls", "rust_analyzer" },
                handlers = {
                    default_setup,
                    lua_ls = function()
                        require("lspconfig").lua_ls.setup {
                            settings = {
                                Lua = {
                                    runtime = { version = "LuaJIT" },
                                    diagnostics = { globals = { "vim" } },
                                    workspace = { library = { vim.env.VIMRUNTIME } }
                                }
                            }
                        }
                    end,
                },
            }
        end,
    },
    {
        -- cmp for completions with snippets and LSs.
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lua",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "max397574/cmp-greek",
            "saadparwaiz1/cmp_luasnip",
        },
        config = function(_, _)
            local cmp = require("cmp")
            cmp.setup {
                snippet = {
                    expand = function(args)
                        require("luasnip").lsp_expand(args.body)
                    end,
                },
                window = {
                    documentation = cmp.config.window.bordered(),
                },
                mapping = cmp.mapping.preset.insert {
                    -- confirm completion item
                    ["<CR>"] = cmp.mapping.confirm({ select = false }),
                    -- toggle completion menu
                    ["<C-e>"] = cmp.mapping.abort(),
                    -- tab complete
                    ["<Tab>"] = cmp.mapping.select_next_item(),
                    ["<S-Tab>"] = cmp.mapping.select_prev_item(),
                    -- scroll documentation window
                    ["<C-f>"] = cmp.mapping.scroll_docs(-5),
                    ["<C-d>"] = cmp.mapping.scroll_docs(5),
                },
                sources = cmp.config.sources({
                    { name = "nvim_lsp" },
                    { name = "luasnip", keyword_length = 2 },
                    { name = "nvim_lua" },
                }, {
                    { name = "greek" },
                    { name = "path" },
                    { name = "buffer", keyword_length = 3 },
                }),
                preselect = "item",
                formatting = {},
                completion = {
                    completeopt = "menu,menuone,noinsert",
                },
            }
        end,
    },
    -- ...and treesitter
    {
        "nvim-treesitter/nvim-treesitter",
        opts = {
            ensure_installed = {
                "c", "lua", "vim", "vimdoc", "query",
                "latex", "markdown", "markdown_inline", "make",
                "julia", "python", "rust", "dockerfile", "xml"
            },
            auto_install = true,
            highlight = {
                disable = { "latex" },
                enable = true,
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = "<C-space>",
                    node_incremental = "<C-space>",
                    scope_incremental = false,
                    node_decremental = "<bs>",
                },
            },
        },
        config = function(_, opts)
            require("nvim-treesitter.configs").setup(opts)
        end,
        lazy = false,
        build = ":TSUpdate"
    },
    -- ...and comments
    {
        "numToStr/Comment.nvim",
        opts = {
            padding = true,
            sticky = true,
            ignore = nil,
            toggler = {
                line = "gcc",
                block = "gbc",
            },
            opleader = {
                line = "gc",
                block = "gb",
            },
            extra = {
                above = "gcO",
                below = "gco",
                eol = "gcA",
            },
            mappings = {
                basic = true,
                extra = true,
            },
        },
        keys = {
            { "gcc", desc = "Comment line",                 mode = "n" },
            { "gbc", desc = "Comment block",                mode = "n" },
            { "gco", desc = "Comment next line and insert", mode = "n" },
            { "gcO", desc = "Comment prev line and insert", mode = "n" },
            { "gcA", desc = "Comment at eol and insert",    mode = "n" },
            { "gc",  desc = "Comment line",                 mode = "v" },
            { "gb",  desc = "Comment block",                mode = "v" },
        },
    },
    -- Interactive code sending and such
    {
        "hkupty/iron.nvim",
        config = function(_, _)
            require("iron.core").setup {
                config = {
                    scratch_repl = true,
                    repl_definition = {
                        sh = { command = { "zsh" } },
                        julia = { command = { "julia", "--project" } },
                        python = { command = { "ipython" } },
                    },
                    repl_open_cmd = require("iron.view").split.vertical.botright(80),
                },
                keymaps = {
                    send_motion = "<leader>sc",
                    visual_send = "<leader>sc",
                    send_file = "<leader>sf",
                    send_line = "<leader>sl",
                    send_mark = "<leader>sm",
                    mark_motion = "<leader>mc",
                    mark_visual = "<leader>mc",
                    remove_mark = "<leader>md",
                    cr = "<leader>s<cr>",
                    interrupt = "<leader>s<leader>",
                    exit = "<leader>sq",
                    clear = "<leader>cl",
                },
                ignore_blank_lines = true,
            }
        end,
        keys = {
            { "<leader>rs", "<Cmd>:IronRepl<CR>",    desc = "Iron REPL" },
            { "<leader>rr", "<Cmd>:IronRestart<CR>", desc = "Iron Restart" },
            { "<leader>rf", "<Cmd>:IronFocus<CR>",   desc = "Iron Focus" },
            { "<leader>rh", "<Cmd>:IronHide<CR>",    desc = "Iron Hide" },
        },
        lazy = true
    },
    {
        -- Help about keybindings while typing keybindings
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
    },
    {
        -- Better wildmenu
        "gelguy/wilder.nvim",
        opts = {
            modes = { ":", "/", "?" }
        },
        config = function(_, opts)
            local wilder = require("wilder")
            wilder.setup(opts)
            wilder.set_option("renderer", wilder.popupmenu_renderer({
                highlighter = wilder.basic_highlighter(),
                max_height = "30%",
            }))
        end,
    },
    -- File formats
    {
        "vim-pandoc/vim-pandoc",
        config = function()
            vim.g["pandoc#syntax#conceal#use"] = 0
            vim.g["pandoc#modules#disabled"] = {
                "folding",
                "bibliographies",
                "autocomplete",
            } -- handled by treesitter and lsp (hopefully)
        end,
        dependencies = {
            "vim-pandoc/vim-pandoc-syntax",
        }
    },
    {
        "dhruvasagar/vim-table-mode",
        config = function()
        end,
        keys = {
            { "<leader>tm", "<Cmd>:TableModeToggle<CR>",    desc = "Toggle Table Mode" },
        }
    },
    "lervag/vimtex",
    "mechatroner/rainbow_csv",
    "julialang/julia-vim",
    "lysogeny/nextflow-vim",
    "imsnif/kdl.vim",
    "qnighy/lalrpop.vim",
    "kaarmu/typst.vim",
    "chikamichi/mediawiki.vim",
    {
        "LhKipp/nvim-nu",
        opts = {
            use_lsp_features = false,
        },
    },
}
require("lazy").setup(plugins)

-- Misc keyboard shortcuts not directly related to plugins
vim.api.nvim_create_autocmd("LspAttach", {
    desc = "Language server actions",
    callback = function(event)
        local wk = require("which-key")
        local opts = { buffer = event.buf }
        wk.register({
            g = {
                d = { "<cmd>lua vim.lsp.buf.definition()<cr>", "Jump to definition", opts },
                D = { "<cmd>lua vim.lsp.buf.declaration()<cr>", "Jump to declaration", opts },
                i = { "<cmd>lua vim.lsp.buf.implementation()<cr>", "List implementations", opts },
                o = { "<cmd>lua vim.lsp.buf.type_definition()<cr>", "Jump to type definition", opts },
                R = { "<cmd>lua vim.lsp.buf.references()<cr>", "List references", opts },
                s = { "<cmd>lua vim.lsp.buf.signature_help()<cr>", "Signature information", opts },
                l = { "<cmd>lua vim.diagnostic.open_float()<cr>", "Floating diagnostics", opts },
            },
            K = { "<cmd>lua vim.lsp.buf.hover()<cr>", "Show function signature", opts },
            ["]d"] = { "<cmd>lua vim.diagnostic.goto_next()<cr>", "Goto next diagnostic", opts },
            ["[d"] = { "<cmd>lua vim.diagnostic.goto_prev()<cr>", "Goto prev diagnostic", opts },
        }, { mode = "n", })
        wk.register({
            ["<F2>"] = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Format buffer", opts },
            ["<F3>"] = { "<cmd>lua vim.lsp.buf.format({async = true})<cr>", "Format buffer", opts },
            ["<F4>"] = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code action", opts },
        }, { mode = { "n", "x", "v" } })
    end,
})

vim.diagnostic.config {
    virtual_text = false,
    severity_sort = true,
    float = {
        style = "minimal",
        border = "rounded",
        source = "always",
        header = "",
        prefix = "",
    },
}

vim.opt.background = "dark"
vim.opt.termguicolors = true
vim.cmd("colo onedark")
vim.opt.completeopt = { "menu", "menuone", "noselect" }

vim.diagnostic.config {
    virtual_text = true
}
-- require("tex") -- TeX
