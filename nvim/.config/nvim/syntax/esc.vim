" vim syntax file
" Language:         ESC/POS markup
" Maintainer:       Ada Loveless
" Last Change:      $Date: 2023/09/15 14:42:09 $
" Version:          $Id: esc.vim,v 1.1 2003/01/28 14:42:09 fugalh Exp $    

if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

syntax keyword escKeyword NUL SOH STX ETX EOT ENQ ACK BEL BS HT LF VT FF CR SO SI DLE DC1 DC2 DC3 DC4 NAK SYN ETB CAN EM SUB ESC FS GS RS US SPACE

syntax match escNumber "\v[0-9]+"
syntax match escHex "\v0x[0-9A-F]+"
syntax match escSeparator " "
syntax match escComment "\v//.*$"
syntax region escCommentBlock start="/\*" end="\*/"
syntax region escString start='"' end='"'

hi link escNumber Constant
hi link escHex Constant
hi link escKeyword Keyword
hi link escComment Comment
hi link escCommentBlock Comment
hi link escString String
