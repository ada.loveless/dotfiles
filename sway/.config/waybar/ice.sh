#!/bin/bash
#
set -euo pipefail

num_to_binary() {
    echo "obase=2;$1" | bc | awk '{printf "%08d", $0}' 
}

ip_to_binary() {
    ip1="$(num_to_binary $(echo $1 | cut -d'.' -f1))"
    ip2="$(num_to_binary $(echo $1 | cut -d'.' -f2))"
    ip3="$(num_to_binary $(echo $1 | cut -d'.' -f3))"
    ip4="$(num_to_binary $(echo $1 | cut -d'.' -f4))"
    echo "$ip1$ip2$ip3$ip4"
}

check_train() {
    route="$(ip r show default | grep -oP '(?<=dev\s)[A-Za-z0-9]+')"
    net="$(ip a show dev ${route} | grep -oP '(?<=inet\s)\d+(\.\d+){3}/\d+')"
    self_ip="$(echo $net | grep -oP '\d+(\.\d+){3}')"
    mask="$(echo $net | grep -oP '(?<=/)\d+')"
    train_ip="$(getent hosts iceportal.de | grep -oP '\d+(\.\d+){3}')"
    train_binary="$(ip_to_binary $train_ip)"
    self_binary="$(ip_to_binary $self_ip)"
    train_trunc="$(head -c $mask <<< $train_binary)"
    self_trunc="$(head -c $mask <<< $self_binary)"
    if [[ "$train_trunc" == "$self_trunc" ]]
    then
        echo "yes"
    else
        echo "no" 
    fi
}

STATUS_ENDPOINT="https://iceportal.de/api1/rs/status"
TRIP_ENDPOINT="https://iceportal.de/api1/rs/tripInfo/trip"


if [[ "$(check_train)" == "yes" ]]
then
    iceportal-status # -p 3030 -a 127.0.0.1 -n
else
    echo -n '{"text": "No train :("}'
fi

# TODO: Detect train by checking IP of iceportal.de
# - ICE portal is in same subnet on train (e.g. 172.18.1.110 iceportal.de with inet 172.18.64.235/16)
# - ICE portal is in internet on internet (e.g. 81.200.196.75 iceportal.de)
