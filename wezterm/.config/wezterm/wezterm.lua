local wezterm = require 'wezterm'

wezterm.on('toggle-ligature', function(window, _)
  local overrides = window:get_config_overrides() or {}
  if not overrides.harfbuzz_features then
    -- If we haven't overridden it yet, then override with ligatures disabled
    overrides.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }
  else
    -- else we did already, and we should disable out override now
    overrides.harfbuzz_features = nil
  end
  window:set_config_overrides(overrides)
end)

--wezterm.on("bell", function(window, pane) 
--    wezterm.run_child_process{"/home/jooa/bin/megalovania"}
--end)

return {
    disable_default_key_bindings = true,
    keys = {
        {
            key = 'E',
            mods = 'CTRL',
            action = wezterm.action.EmitEvent 'toggle-ligature' -- |> => <= <| 
        },
        { key = "-", mods = "CTRL", action = wezterm.action.DecreaseFontSize },
        { key = "=", mods = "CTRL", action = wezterm.action.IncreaseFontSize },
        { key = ")", mods = "CTRL", action = wezterm.action.ResetFontSize },
        { key = "V", mods = "CTRL", action = wezterm.action.PasteFrom("Clipboard") },
        { key = "C", mods = "CTRL", action = wezterm.action.CopyTo("Clipboard") },
    },
    window_background_opacity = 0.9,
    colors = {
        foreground = '#ffffff',
        background = '#111111',
        cursor_bg = '#ffffff',
        cursor_fg = '#000000',
        --cursor_border = '#111111',
        selection_fg = 'black',
        selection_bg = '#fffacd',
        ansi = {
            '#101012', -- 0
            '#de5d68', -- 1
            '#8ad454', -- 2
            '#ead050', -- 3
            '#47a5e5', -- 4
            '#bb70d2', -- 6
            '#51a8b3', -- 7
            '#848484', -- 8
            -- 7  #848484
        },
        brights = {
            '#a7aab0', -- 9
            '#a34141', -- 10
            '#638848', -- 11
            '#cdc322', -- 12
            '#1e77c1', -- 13
            '#79428a', -- 14
            '#328f96', -- 15
            '#5c5c5c', -- 16
        },
        visual_bell = "#555555",
    },
    font = wezterm.font 'FiraCode Nerd Font',
    font_size = 9.0,
    enable_tab_bar = false,
    adjust_window_size_when_changing_font_size = false,
    automatically_reload_config = true,
    bold_brightens_ansi_colors = false,
    audible_bell = "Disabled",
    window_padding = {
        left = 2,
        right = 2,
        bottom = 2,
        top = 2,
    },
    enable_scroll_bar = false,
    enable_wayland = true,
    window_close_confirmation = "NeverPrompt",
}
